﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoughtworksPrelim.Contract;

namespace ThoughtworksPrelim
{
    internal class Program
    {
        internal static void Main(string[] args)
        {
            var goods = JsonLoader.LoadGoods("Goods.json");
            var sessions = JsonLoader.LoadSessions("Sessions.json");
            var promotions = JsonLoader.LoadPromotions("Promotions.json");
            // 如果包含了多组收银机数据，在这里我们就选择第一个吧。
            var session = sessions[0];
            var receiptManager = new RecieptManager(new PromotionDiscountProvider(promotions))
            {
                StoreName = "Alderpaw's Store"
            };
            var receipt = receiptManager.BuildReceipt(session, goods);
            receiptManager.WriteReciept(receipt, Console.Out);
        }
    }
}
