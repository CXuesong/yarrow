﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoughtworksPrelim.Contract;

namespace ThoughtworksPrelim
{
    /// <summary>
    /// 表示一张购物小票。包含商品条目及折扣信息。
    /// </summary>
    public class Receipt
    {
        public Receipt(IList<ReceiptEntry> entries)
        {
            Entries = entries;
        }

        /// <summary>
        /// 商品条目。
        /// </summary>
        public IList<ReceiptEntry> Entries { get; }
    }

    /// <summary>
    /// 小票中的一项。包含商品、数量以及折扣信息。
    /// </summary>
    public class ReceiptEntry
    {
        public ReceiptEntry(GoodsEntry goods, int amount)
        {
            if (goods == null) throw new ArgumentNullException(nameof(goods));
            Goods = goods;
            Amount = amount;
            TotalPrice = (decimal) Goods.Price* amount;
        }

        /// <summary>
        /// 商品信息。
        /// </summary>
        public GoodsEntry Goods { get; }

        /// <summary>
        /// 商品的数量。
        /// </summary>
        public int Amount { get; }

        /// <summary>
        /// 优惠前的总价。
        /// </summary>
        public decimal TotalPrice { get; }

        /// <summary>
        /// 此条目参与的折扣优惠。
        /// </summary>
        public IList<AppliedDiscount> Discounts { get; } = new List<AppliedDiscount>();

        /// <summary>
        /// 折扣优惠价格。
        /// </summary>
        public decimal DiscountPrice => Discounts.Sum(d => d.DiscountPrince);

        /// <summary>
        /// 优惠后的总价。
        /// </summary>
        public decimal ActualPrice => TotalPrice - Discounts.Sum(d => d.DiscountPrince);
    }

    /// <summary>
    /// 对小票项目已经应用的折扣信息。
    /// </summary>
    public class AppliedDiscount
    {
        public AppliedDiscount(string discountType, decimal discountPrince)
        {
            if (discountType == null) throw new ArgumentNullException(nameof(discountType));
            DiscountType = discountType;
            DiscountPrince = discountPrince;
        }

        /// <summary>
        /// 折扣（discount / promotion）类型。
        /// </summary>
        public string DiscountType { get; }

        /// <summary>
        /// 折扣优惠的价格。
        /// </summary>
        public decimal DiscountPrince { get; }
    }
}
