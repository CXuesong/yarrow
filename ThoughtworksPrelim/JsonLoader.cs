﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ThoughtworksPrelim.Contract;

namespace ThoughtworksPrelim
{
    /// <summary>
    /// 用于从文件/流中载入 JSON 数据。
    /// </summary>
    public static class JsonLoader
    {
        private static readonly JsonSerializer serializer = new JsonSerializer();

        public static GoodsCollection LoadGoods(string path)
        {
            return LoadGoods(File.OpenText(path));
        }

        public static GoodsCollection LoadGoods(TextReader reader)
        {
            return (GoodsCollection) serializer.Deserialize(reader, typeof (GoodsCollection));
        }

        public static IList<Session> LoadSessions(string path)
        {
            return LoadSessions(File.OpenText(path));
        }

        public static IList<Session> LoadSessions(TextReader reader)
        {
            return (IList<Session>) serializer.Deserialize(reader, typeof (IList<Session>));
        }


        public static PromotionCollection LoadPromotions(string path)
        {
            return LoadPromotions(File.OpenText(path));
        }

        public static PromotionCollection LoadPromotions(TextReader reader)
        {
            return (PromotionCollection) serializer.Deserialize(reader, typeof (PromotionCollection));
        }
    }
}
