﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoughtworksPrelim
{
    /// <summary>
    /// 表示一组可按照条形码检索的商品集合。
    /// </summary>
    public class GoodsCollection : KeyedCollection<string, GoodsEntry>
    {
        /// <summary>
        /// 在派生类中实现时，将从指定元素提取键。
        /// </summary>
        /// <returns>
        /// 指定元素的键。
        /// </returns>
        /// <param name="item">从中提取键的元素。</param>
        protected override string GetKeyForItem(GoodsEntry item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            return item.Barcode;
        }
    }

    /// <summary>
    /// 用于描述商品的基本信息。
    /// </summary>
    public class GoodsEntry
    {
        public string Barcode { get; set; }

        public string Name { get; set; }

        public string Unit { get; set; }

        public string Category { get; set; }

        public string SubCategory { get; set; }

        public double Price { get; set; }
    }
}
