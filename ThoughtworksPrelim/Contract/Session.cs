﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ThoughtworksPrelim.Contract
{
    /// <summary>
    /// 用于表示收银机一次返回的商品项目洌表。
    /// </summary>
    /// <remarks>
    /// 列表中的内容不保证不重复。即同一商品可能多次出现。
    /// </remarks>
    public class Session : List<SessionEntry>
    {

    }

    /// <summary>
    /// 收银机项目，包含了商品条码及其数量。
    /// </summary>
    public class SessionEntry
    {
        /// <summary>
        /// 商品的条形码。
        /// </summary>
        public string Barcode { get; set; }

        /// <summary>
        /// 商品的数量。
        /// </summary>
        public int Amount { get; set; }

        // JSON 单向转换支持。
        public static explicit operator SessionEntry(string expr)
        {
            if (expr == null) return null;
            var parts = expr.Split('-');
            return new SessionEntry(parts[0],
                parts.Length > 1 ? Convert.ToInt32(parts[1]) : 1);
        }

        public SessionEntry()
        {
            
        }

        public SessionEntry(string barcode, int amount)
        {
            Barcode = barcode;
            Amount = amount;
        }
    }
}
