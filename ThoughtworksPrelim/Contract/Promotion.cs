﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoughtworksPrelim.Contract
{
    public class PromotionCollection : KeyedCollection<string, Promotion>
    {
        /// <summary>
        /// 在派生类中实现时，将从指定元素提取键。
        /// </summary>
        /// <returns>
        /// 指定元素的键。
        /// </returns>
        /// <param name="item">从中提取键的元素。</param>
        protected override string GetKeyForItem(Promotion item)
        {
            if (item == null) throw new ArgumentNullException(nameof(item));
            return item.Type;
        }

        /// <summary>
        /// 尝试获取指定类型的促销信息。
        /// </summary>
        /// <returns>指定类型的促销信息。如果不存在，则返回<c>null</c>。</returns>
        /// <exception cref="ArgumentNullException"><paramref name="promotionType"/>为<c>null</c>。</exception>
        public Promotion TryGetPromotion(string promotionType)
        {
            if (Dictionary == null)
            {
                return Items.FirstOrDefault(i => GetKeyForItem(i) == promotionType);
            }
            else
            {
                Promotion p;
                if (Dictionary.TryGetValue(promotionType, out p)) return p;
                return null;
            }
        }
    }

    /// <summary>
    /// 表示一条促销政策。
    /// </summary>
    public class Promotion
    {
        public Promotion()
        {
            
        }

        public Promotion(string type, params string[] barcodes)
        {
            if (type == null) throw new ArgumentNullException(nameof(type));
            Type = type;
            Barcodes = new List<string>(barcodes);
        }

        /// <summary>
        /// 促销类型标识。
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// 应当应用促销的商品条形码。
        /// </summary>
        public IList<string> Barcodes { get; set; }
    }
}
