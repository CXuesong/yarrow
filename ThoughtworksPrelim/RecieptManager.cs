﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoughtworksPrelim.Contract;

namespace ThoughtworksPrelim
{
    /// <summary>
    /// 用于实现购物小计和小票的打印操作。
    /// </summary>
    public class RecieptManager
    {
        /// <param name="discountProvider">用于提供优惠决策。不可为 <c>null</c> 。</param>
        public RecieptManager(IDiscountProvider discountProvider)
        {
            if (discountProvider == null) throw new ArgumentNullException(nameof(discountProvider));
            DiscountProvider = discountProvider;
        }

        /// <summary>
        /// 商店名称。
        /// </summary>
        public string StoreName { get; set; }

        /// <summary>
        /// 金额单位。
        /// </summary>
        public string MonetaryUnit { get; set; } = "元";

        public IDiscountProvider DiscountProvider { get; }

        /// <summary>
        /// 根据收银机项目和商品列表，生成小票，并应用减价策略。
        /// </summary>
        public Receipt BuildReceipt(Session session, GoodsCollection goods)
        {
            var mergedSession = session.GroupBy(e => e.Barcode)
                .Select(g => new SessionEntry(g.Key, g.Sum(e => e.Amount)));
            var receipt = new Receipt(mergedSession.Select(se =>
                new ReceiptEntry(goods[se.Barcode], se.Amount)).ToList());
            DiscountProvider.ApplyDiscounts(receipt);
            return receipt;
        }

        /// <summary>
        /// 向指定的 <see cref="TextWriter"/> 中写入小票内容。
        /// </summary>
        public void WriteReciept(Receipt receipt, TextWriter writer)
        {
            const string hr1 = "--------------------------";
            const string hr2 = "**************************";
            if (receipt == null) throw new ArgumentNullException(nameof(receipt));
            if (writer == null) throw new ArgumentNullException(nameof(writer));
            writer.WriteLine("*** {0} 购物清单 ***", StoreName);
            var total = 0m;
            var saved = 0m;
            foreach (var e in receipt.Entries)
            {
                writer.Write("名称：{0}，数量：{1}{2}，单价：{3:F2}（{4}），小计{5:F2}（{4}）",
                    e.Goods.Name, e.Amount, e.Goods.Unit,
                    e.Goods.Price, MonetaryUnit, e.ActualPrice);
                total += e.ActualPrice;
                if (e.Discounts.Count > 0)
                {
                    var dp = e.DiscountPrice;
                    writer.Write("，优惠：{0:F2}（{1}）", dp, MonetaryUnit);
                    saved += dp;
                }
                writer.WriteLine();
            }
            writer.WriteLine(hr1);
            // 对每一类优惠，分别输出优惠信息。
            foreach (var g in receipt.Entries
                .SelectMany(e => e.Discounts, (e, d) => new {Entry = e, Discount = d})
                .GroupBy(p => p.Discount.DiscountType))
            {
                writer.WriteLine("{0}商品：", DiscountProvider.GetFriendlyName(g.Key));
                foreach (var p in g)
                {
                    writer.WriteLine("名称：{0}，数量：{1}{2}",
                        p.Entry.Goods.Name, p.Entry.Amount, p.Entry.Goods.Unit);
                }
                writer.WriteLine(hr1);
            }
            writer.WriteLine("总计：{0}（{1}）", total, MonetaryUnit);
            if (saved > 0)
                writer.WriteLine("节省：{0:F2}（{1}）", saved, MonetaryUnit);
            writer.WriteLine(hr2);
        }
    }
}
