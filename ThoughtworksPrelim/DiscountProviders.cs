﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoughtworksPrelim.Contract;

namespace ThoughtworksPrelim
{
    /// <summary>
    /// 为减价优惠提供程序提供公共接口。
    /// </summary>
    public interface IDiscountProvider
    {
        /// <summary>
        /// 对指定的小票应用减价优惠。
        /// </summary>
        void ApplyDiscounts(Receipt receipt);

        /// <summary>
        /// 获取指定减价优惠类型的友好名称。
        /// </summary>
        string GetFriendlyName(string discountType);
    }

    /// <summary>
    /// 一个不会提供任何优惠的减价提供程序。
    /// </summary>
    public class EmptyDiscountProvider : IDiscountProvider
    {
        /// <summary>
        /// 一个默认的 <see cref="EmptyDiscountProvider"/> 实例。
        /// </summary>
        public static readonly EmptyDiscountProvider Default = new EmptyDiscountProvider();

        /// <summary>
        /// 对指定的小票应用减价优惠。
        /// </summary>
        public void ApplyDiscounts(Receipt receipt)
        {
            // Provide no discounts.
        }

        /// <summary>
        /// 获取指定减价优惠类型的友好名称。
        /// </summary>
        public string GetFriendlyName(string discountType)
        {
            return discountType;
        }
    }

    /// <summary>
    /// 促销减价提供程序。
    /// </summary>
    public class PromotionDiscountProvider : IDiscountProvider
    {
        private readonly PromotionCollection _Promotions;

        /// <summary>
        /// 买二送一 的促销类型名称。
        /// </summary>
        public const string BuySecondGetHalfPriceDiscountType = "BUY_SECOND_GET_HALF_PRICE";

        public PromotionDiscountProvider(PromotionCollection promotions)
        {
            if (promotions == null) throw new ArgumentNullException(nameof(promotions));
            _Promotions = promotions;
        }

        /// <summary>
        /// 对指定的小票应用促销。
        /// </summary>
        public void ApplyDiscounts(Receipt receipt)
        {
            if (receipt == null) throw new ArgumentNullException(nameof(receipt));
            var p = _Promotions.TryGetPromotion(BuySecondGetHalfPriceDiscountType);
            var h = new HashSet<string>(p.Barcodes);
            if (p != null)
            {
                foreach (var e in receipt.Entries)
                {
                    if (e.Amount >= 2 && h.Contains(e.Goods.Barcode))
                        e.Discounts.Add(new AppliedDiscount(BuySecondGetHalfPriceDiscountType,
                            (e.Amount/2)*(decimal) e.Goods.Price));
                }
            }
        }

        /// <summary>
        /// 获取指定促销类型的友好名称。
        /// </summary>
        public string GetFriendlyName(string discountType)
        {
            switch (discountType)
            {
                case BuySecondGetHalfPriceDiscountType:
                    return "第二个半价";
                default:
                    return discountType;
            }
        }
    }
}
