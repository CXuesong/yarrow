﻿using System;
using System.Diagnostics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ThoughtworksPrelim;

namespace UnitTestProject1
{
    [TestClass]
    public class JsonLoaderTests
    {
        [TestMethod]
        public void LoadGoodsTest()
        {
            var goods = JsonLoader.LoadGoods("Goods.json");
            Assert.AreEqual(2, goods.Count);
            Assert.AreEqual("A", goods[0].Barcode);
            Assert.AreEqual("B", goods[1].Barcode);
            Trace.WriteLine(Utility.ObjectToString(goods[0]));
            Trace.WriteLine(Utility.ObjectToString(goods[1]));
        }

        [TestMethod]
        public void LoadSessionsTest()
        {
            var sessions = JsonLoader.LoadSessions("Sessions.json");
            Assert.AreEqual(2, sessions.Count);
            Assert.AreEqual(6, sessions[0].Count);
            Assert.AreEqual(0, sessions[1].Count);
            Trace.WriteLine(Utility.ObjectToString(sessions[0]));
            Trace.WriteLine(Utility.ObjectToString(sessions[1]));
        }
    }
}
