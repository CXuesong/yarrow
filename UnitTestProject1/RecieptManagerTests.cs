﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ThoughtworksPrelim;
using ThoughtworksPrelim.Contract;

namespace UnitTestProject1
{
    [TestClass]
    public class RecieptManagerTests
    {
        private readonly GoodsCollection goods = new GoodsCollection
        {
            new GoodsEntry
            {
                Barcode = "A",
                Category = "C1",
                Name = "Good1",
                Price = 2.0,
                SubCategory = "SC1",
                Unit = "U"
            },
            new GoodsEntry
            {
                Barcode = "B",
                Category = "C1",
                Name = "Good2",
                Price = 1.0,
                SubCategory = "SC1",
                Unit = "U"
            },
            new GoodsEntry
            {
                Barcode = "C",
                Category = "C1",
                Name = "Good3",
                Price = 3.0,
                SubCategory = "SC2",
                Unit = "U"
            },
        };

        private readonly Session session = new Session()
        {
            new SessionEntry("A", 5),
            new SessionEntry("B", 1),
            new SessionEntry("C", 9),
            new SessionEntry("A", 2),
        };

        /// <summary>
        /// 为指定的一个商品提供减价策略。
        /// </summary>
        private class SpecificGoodsDiscountProvider : IDiscountProvider
        {
            private readonly string _DiscountedGoodsBarcode;

            public SpecificGoodsDiscountProvider(string discountedGoodsBarcode)
            {
                if (discountedGoodsBarcode == null) throw new ArgumentNullException(nameof(discountedGoodsBarcode));
                _DiscountedGoodsBarcode = discountedGoodsBarcode;
            }

            /// <summary>
            /// 对指定的小票应用促销。
            /// </summary>
            public void ApplyDiscounts(Receipt receipt)
            {
                foreach (var e in receipt.Entries)
                {
                    if (e.Goods.Barcode == _DiscountedGoodsBarcode)
                        e.Discounts.Add(new AppliedDiscount("SPECIAL_FREE_ACTION", e.TotalPrice));
                }
            }

            /// <summary>
            /// 获取指定促销类型的友好名称。
            /// </summary>
            public string GetFriendlyName(string discountType)
            {
                if (discountType == "SPECIAL_FREE_ACTION") return "“指定商品免费行动”";
                return discountType;
            }
        }

        [TestMethod]
        public void BuildReceiptTest()
        {
            var rm = new RecieptManager(EmptyDiscountProvider.Default);
            var receipt = rm.BuildReceipt(session, goods);
            var barcodes = session.GroupBy(e => e.Barcode).Select(g => g.Key).OrderBy(c => c).ToArray();
            // 小票项目应当不重复。
            Assert.IsTrue(barcodes.SequenceEqual(receipt.Entries.Select(r => r.Goods.Barcode).OrderBy(c => c)));
            foreach (var e in receipt.Entries)
            {
                Assert.AreEqual(e.Goods.Price*e.Amount, (double) e.TotalPrice);
                Assert.AreEqual(e.ActualPrice + e.DiscountPrice, e.TotalPrice);
            }
        }

        [TestMethod]
        public void DiscountTest()
        {
            const string discountedItem = "A";
            const decimal eps = 1e-8m;
            // A is totally free today!
            var rm = new RecieptManager(new SpecificGoodsDiscountProvider(discountedItem));
            var receipt = rm.BuildReceipt(session, goods);
            foreach (var e in receipt.Entries)
            {
                var shouldBeDiscounted = e.Goods.Barcode == discountedItem;
                Assert.AreEqual(shouldBeDiscounted, Math.Abs(e.ActualPrice) < eps, $"商品{e.Goods.Barcode}，实际价格{e.ActualPrice}。");
            }
        }

        [TestMethod]
        public void PromotionDiscountTest()
        {
            const double eps = 1e-8;
            var discountedItems = new[] {"A", "B"};
            var promotionDiscount = new PromotionDiscountProvider(new PromotionCollection
            {
                new Promotion(PromotionDiscountProvider.BuySecondGetHalfPriceDiscountType, discountedItems)
            }
                );
            var rm = new RecieptManager(promotionDiscount);
            var receipt = rm.BuildReceipt(session, goods);
            foreach (var e in receipt.Entries)
            {
                var shouldBeDiscounted = discountedItems.Contains(e.Goods.Barcode);
                var actualPrice = e.Goods.Price*e.Amount;
                if (shouldBeDiscounted)
                {
                    // We use a safer approach here.
                    if (e.Amount%2 == 0) actualPrice -= e.Goods.Price*e.Amount/2;
                    else actualPrice -= e.Goods.Price*(e.Amount - 1)/2;
                }
                Assert.AreEqual((double) e.ActualPrice, actualPrice, eps,
                    $"商品{e.Goods.Barcode}，实际价格{e.ActualPrice}。");
            }
        }

        [TestMethod]
        public void WriteReceiptTest()
        {
            var rm = new RecieptManager(new SpecificGoodsDiscountProvider("B"));
            var receipt = rm.BuildReceipt(session, goods);
            using (var sw = new StringWriter())
            {
                rm.WriteReciept(receipt, sw);
                Trace.WriteLine(sw.ToString());
            }
        }
    }
}