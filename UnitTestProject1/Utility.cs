﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestProject1
{
    internal static class Utility
    {
        public static string ObjectToString(object value)
        {
            if (value == null) return "null";
            var sb = new StringBuilder(value.GetType().ToString());
            sb.Append('\n');
            var c = value as IEnumerable;
            if (c != null)
            {
                var i = 0;
                foreach (var item in c)
                {
                    sb.AppendFormat("[{0}]", i);
                    sb.Append(ObjectToString(item));
                    i++;
                }
            }
            else
            {
                foreach (var p in value.GetType().GetProperties())
                    sb.AppendFormat("\t{0} = {1}\n", p.Name, p.GetValue(value));
            }
            return sb.ToString();
        }
    }
}
